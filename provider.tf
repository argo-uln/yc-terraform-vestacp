terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = file(var.ya_account_key)
  cloud_id                 = var.ya_cloud_id
  folder_id                = var.ya_folder_id
  zone                     = var.ya_zone
}
