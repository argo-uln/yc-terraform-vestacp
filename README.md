## Установка хостинг панели VestaCP при помощи Terraform и Ansible в облаке Yandex.
> VESTA Control Panel - это простая в установке и настройке панель управления хостингом

## Описание
Проект содержит скрипты по автоматическому разворачиванию VestaCP в облаке Яндекс.  

## Предварительная настройка
>Для поддержки ДНС регистаторы требуют указывать как минимум два ДНС сервера. Для установки панели в нашем случае была выбрана ОС Ubuntu 18.04. Для этой ОС шаблон установки ВМ позволяет использовать только один внешний IP адрес. Для решения этой проблемы мной было отправлено в поддержку Яндекс письмо следующего содержания. Прошу активировать возможность использования нескольких сетевых интерфейсов.


- Прежде всего Вам потребуется установить на локальной машине поддержку `Terraform` и `Ansible`.
- Склонировать репозиторий `git clone https://gitlab.com/argo-uln/yc-terraform-vestacp.git`
- Переименовать файл `terraform.tfvars.example` в `terraform.tfvars`
- Добавить файл `key.json`
- Поправить содержимое переменных в файле `terraform.tfvars`


## Установка

```
terraform init
terraform plan
terraform apply
```

- Открыть в браузере https://ваш_ip:8083
- Пользователь: `Admin`
- Пароль: значение `password` из `terraform.tfvars`

- Terraform разворачивает виртуалку с двумя внешними ip и двумя внутренними адресами. Работа через nat.
- При помощи Ansible настраивается второй ip адрес.
- Настраиваются маршруты на основе политик, делается видимым второй интерфейс снаружи.
- Устанавливается панель VestaCP.

## Описание переменных
```
ya_account_key = "service_account_key_file.json"
ya_cloud_id = ""
ya_folder_id = ""
ya_zone = "ru-central1-a"
ya_platform_id = "standard-v2"
my_ssh_key = "~/.ssh/id_rsa.pub"
private_key = "~/.ssh/id_rsa"
devs = "argo"
```


- Ниже переменные относящиеся к скрипту установки Панели.
- При ОЗУ менее 7гБ не рекомендуется включать `Spamassassin` и `ClamAV`.
- Одновременно нельзя использовать `apache` и `phpfpm`, `vsftp` и `proftp`, `mysql` и `postgresql`.
- Переменная hostname содержит доменное имя вашего хоста.

```
nginx = "yes"
apache = "yes"
phpfpm = "no"
named = "yes"
remi = "no"
vsftp = "yes"
proftp = "no"
iptables = "yes"
fail2ban = "yes"
quota = "no"
exim = "yes"
dovecot = "yes"
spamassassin = "no"
clamav = "no"
softaculous = "no"
mysql = "yes"
postgresql = "no"
hostname = "host.domain.com"
email = "email@domain.com"
password = "my_Passw0rd!_for_VestaCp_panel"
port = "8083"
```


## HOWTO

## Поддержка

## Authors and acknowledgment

## License

MIT License

## Project status
- Панель установлена и настроена.
- Не получилось отправлять письма, ограничение Хостера(для открытия 25 порта требуется обращение в поддержку - проверить).
- Для основного ip адреса необходимо прописать обратную зону - возможно ли это?   
