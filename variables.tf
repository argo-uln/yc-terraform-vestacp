variable "ya_account_key" {
}
variable "ya_cloud_id" {
}
variable "ya_folder_id" {
}
variable "ya_zone" {
}
variable "ya_platform_id" {
}
variable "my_ssh_key" {
  default = "~/.ssh/id_rsa.pub"
}
variable "private_key" {
  default = "~/.ssh/id_rsa"
}
variable "devs" {
  default = "argo"
}
variable "nginx" {
  default = "yes"
}
variable "apache" {
  default = "yes"
}
variable "phpfpm" {
  default = "no"
}
variable "named" {
  default = "yes"
}
variable "remi" {
  default = "no"
}
variable "vsftp" {
  default = "yes"
}
variable "proftp" {
  default = "no"
}
variable "iptables" {
  default = "yes"
}
variable "fail2ban" {
  default = "yes"
}
variable "quota" {
  default = "no"
}
variable "exim" {
  default = "yes"
}
variable "dovecot" {
  default = "yes"
}
variable "spamassassin" {
  default = "no"
}
variable "clamav" {
  default = "no"
}
variable "softaculous" {
  default = "no"
}
variable "mysql" {
  default = "yes"
}
variable "postgresql" {
  default = "no"
}
variable "hostname" {
}
variable "email" {
}
variable "password" {
}
variable "port" {
  default = "8083"
}
