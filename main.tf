data "yandex_compute_image" "ubuntu" {
  family = "ubuntu-1804-lts"
}

resource "yandex_vpc_network" "my-vpc" {
  name = "my-vpc"
}

resource "yandex_vpc_subnet" "eth0-subnet" {
  name           = "eth0-subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.my-vpc.id
  v4_cidr_blocks = ["192.168.1.0/24"]
}

resource "yandex_vpc_subnet" "eth1-subnet" {
  name           = "eth1-subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.my-vpc.id
  v4_cidr_blocks = ["192.168.2.0/24"]
}

resource "yandex_compute_instance" "vm" {
  name        = var.devs
  hostname    = var.devs
  platform_id = var.ya_platform_id

  resources {
    cores         = 2
    memory        = 1
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      size     = 50
      image_id = data.yandex_compute_image.ubuntu.id
    }
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.eth0-subnet.id
    ip_address = "192.168.1.3"
    nat        = true
  }

  network_interface {
    subnet_id  = yandex_vpc_subnet.eth1-subnet.id
    ip_address = "192.168.2.3"
    nat        = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.my_ssh_key)}"
  }
}

resource "local_file" "inventory" {
  content = templatefile(
    "inventory.tftpl",
    {
      name         = var.devs,
      ip           = yandex_compute_instance.vm.network_interface.0.nat_ip_address,
      p_key        = var.private_key,
      v_nginx        = var.nginx,
      v_apache       = var.apache,
      v_phpfpm       = var.phpfpm,
      v_named        = var.named,
      v_remi         = var.remi,
      v_vsftp        = var.vsftp,
      v_proftp       = var.proftp,
      v_iptables     = var.iptables,
      v_fail2ban     = var.fail2ban,
      v_quota        = var.quota,
      v_exim         = var.exim,
      v_dovecot      = var.dovecot,
      v_spamassassin = var.spamassassin,
      v_clamav       = var.clamav,
      v_softaculous  = var.softaculous,
      v_mysql        = var.mysql,
      v_postgresql   = var.postgresql,
      v_hostname     = var.hostname,
      v_email        = var.email,
      v_password     = var.password,
      v_port         = var.port
    }
  )
  filename = "inventory.yaml"
}

resource "null_resource" "ansible" {
  depends_on = [local_file.inventory]
  provisioner "local-exec" {
    environment = {
      ANSIBLE_HOST_KEY_CHECKING = "False"
    }
    command = "ansible-playbook playbook.yaml -i inventory.yaml"
  }
}
